
import 'package:youtube/models/channel.dart';
import 'package:youtube/models/video.dart';
import 'package:youtube/network/network_manager.dart';
import 'package:youtube/utilities/keys.dart';

class ApiService {
  NetworkManager dio = NetworkManager();

  var nextPageToken = '';

  Future<Channel> fetchChannel({String channelId}) async {
    var response = await dio.getHttp(
        'youtube/v3/channels?part=snippet&part=contentDetails&part=statistics&id=$channelId&key=$apiKey');
    if (response.statusCode == 200) {
      Channel channel = Channel.fromMap(response.data['items'][0]);
      channel.videos =
          await fetchVideosFromPlayList(playListId: channel.uploadPlaylistId);
      return channel;
    } else {
      return null;
    }
  }

  // ignore: missing_return
  Future<List<Video>> fetchVideosFromPlayList({String playListId}) async {
    var response = await dio.getHttp(
        '/youtube/v3/playlistItems?part=snippet&playlistId=$playListId&maxResults=8&pageToken=$nextPageToken&key=$apiKey');
    if (response.statusCode == 200) {
      nextPageToken = response.data['nextPageToken'] ?? '';
      List<dynamic> videosJson = response.data['items'];

      List<Video> videos = [];

      videosJson.forEach((json) => videos.add(Video.fromMap(json['snippet'])));

      return videos;
    }
  }
}
