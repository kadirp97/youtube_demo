
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide Router;
import 'package:youtube/models/channel.dart';
import 'package:youtube/services/api_service.dart';



class HomeViewModel  {
  ApiService apiService = ApiService();
  // ignore: always_declare_return_types
  Future<Channel> fetchChannel({@required channelId}) async {

    var result = await apiService.fetchChannel(channelId: channelId);

    if (result!=null) {
      return result;
    } else {
      return null;
    }
  }
}
