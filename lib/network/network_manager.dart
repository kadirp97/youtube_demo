import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class NetworkManager {
  Dio _dio;

  NetworkManager() {
    final baseOptions = BaseOptions(
      baseUrl: 'https://www.googleapis.com/',

      headers: {
        'Content-type': 'application/json'
      },
    );

    _dio = Dio(baseOptions);

  }

  Future<Response> postHttp({@required String path, @required Map body}) async {
    try {
      return await _dio.post(path, data: body);
    } catch (e) {
      if (e is DioError) {
        return e.response;
      }
      return null;
    }
  }
  Future<Response> postHttpFormData({@required String path, @required FormData formData}) async {
    try {
      return await _dio.post(path, data: formData);
    } catch (e) {
      if (e is DioError) {
        return e.response;
      }
      return null;
    }
  }

  Future<Response> getHttp(String path) async {
    try {
      var response=await _dio.get(path);
      if(response.data['error_code']==0){

      }
      return await _dio.get(path);
    } catch (e) {
      if (e is DioError) {
        return e.response;
      }
      return null;
    }
  }

  Future<Response> patchHttp(
      {@required String path, @required Map body}) async {
    try {
      return await _dio.patch(path, data: body);
    } catch (e) {
      return null;
    }
  }

  Future<Response> putHttp({@required String path, @required Map body}) async {
    try {
      return await _dio.put(path, data: body);
    } catch (e) {
      return null;
    }
  }

  Future<Response> deleteHttp(String path) async {
    try {
      return await _dio.delete(path);
    } catch (e) {
      return null;
    }
  }
}
